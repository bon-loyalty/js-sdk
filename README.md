<p align="center" width="100%">
  <a href="https://bonloyalty.com" target="_blank"><img width="33%" src="https://bonloyalty.com/wp-content/uploads/2022/04/bon-loyalty.svg" alt="BON Loyalty is a trusted rewards and referrals solution provider that helps merchants increase customer engagement with captivating points, rewards & referral program."></a>
</p>

# BON Loyalty JS SDK Document



## Getting started

Install [BON Loyalty app](https://apps.shopify.com/bon-loyalty-rewards?utf=js-sdk) on your Shopify Store and enable Widget on your Storefront and make sure Widget loaded correctly on Storefront!
![BON Loyalty Storefront Widget](/images/BON-Loyalty-storefront-widget.png "BON Loyalty Storefront Widget")


## SKD Supported Events

```
widget_btn : On Widget Icon button Clicked
customer_join : On Join button clicked
customer_signin : On Sign-in button clicked
earning_create_an_account : On Create Account button clicked 
earning_complete_an_order : On Complete An Order button clicked
earning_subscribe_for_newsletter : On Subscribe Newsletter button clicked
earning_happy_birthday_save : On Birthday Save button clicked
earning_leave_a_review : On Leave A Review button clicked
earning_follow_on_instagram : On Follow On Instagram button clicked
earning_follow_on_twitter : On Follow On Twitter button clicked
earning_retweet : On Retweet button clicked
earning_share_on_twitter : On Share On Twitter button clicked
earning_like_on_facebook : On Like On Facebook button clicked
earning_share_on_facebook : On Share On Facebook button clicked
earning_complete_profile : On Complete Profile button clicked
earning_follow_on_tiktok : On Follow On Tiktok button clicked
earning_share_on_linkedin : On Share On Linkedin button clicked
earning_follow_on_linkedin : On Follow On Linkedin button clicked
earning_join_a_facebook_group : On Join A Facebook Group button clicked
earning_subscribe_on_youtube : On Subscribe On Youtube button clicked
earning_follow_on_pinterest : On Follow On Pinterest button clicked
redeem_apply_now : On Redeem Apply Now button clicked
myreward_use_it_now : On My Reward - Use It Now button clicked
viptier_purchase_more : On Purchase More button clicked
```

## window.BON_SDK_DATA
This is BON Loyalty Javascript event fired within user's interactions on BON Loyalty Widget on merchant storefront.  

Simply inject your custom code on ***// YOUR CUSTOM CODE GOES HERE*** as below Working Sample section to listen to BON Loyayty Widget events and able to execute your own Javascript code.

## Working Samples

On liquid template code
```js
  <head>
  <script>
    window.BON_SDK_DATA = {
        events: {
            widget_btn: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('widget_btn, data: ', data)
                },
                run_original_function: false
            },
            customer_join: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('customer_join, data: ', data)
                },
            },
            customer_signin: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('customer_signin, data: ', data)
                },
            },
            earning_create_an_account: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('earning_create_an_account, data: ', data)
                },
            },
            earning_complete_an_order: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('earning_complete_an_order, data: ', data)
                },
            },
            earning_subscribe_for_newsletter: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('earning_subscribe_for_newsletter, data: ', data)
                },
            },
            earning_happy_birthday_save: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('earning_happy_birthday_save, data: ', data)
                },
            },
            earning_leave_a_review: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('earning_leave_a_review, data: ', data)
                },
            },
            earning_follow_on_instagram: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('earning_follow_on_instagram, data: ', data)
                },
            },
            earning_follow_on_twitter: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('earning_follow_on_twitter, data: ', data)
                },
            },
            earning_retweet: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('earning_retweet, data: ', data)
                },
            },
            earning_share_on_twitter: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('earning_share_on_twitter, data: ', data)
                },
            },
            earning_like_on_facebook: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('earning_like_on_facebook, data: ', data)
                },
            },
            earning_share_on_facebook: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('earning_share_on_facebook, data: ', data)
                },
            },
            earning_complete_profile: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('earning_complete_profile, data: ', data)
                },
            },
            earning_follow_on_tiktok: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('earning_follow_on_tiktok, data: ', data)
                },
            },
            earning_share_on_linkedin: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('earning_share_on_linkedin, data: ', data)
                },
            },
            earning_follow_on_linkedin: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('earning_follow_on_linkedin, data: ', data)
                },
            },
            earning_join_a_facebook_group: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('earning_join_a_facebook_group, data: ', data)
                },
            },
            earning_subscribe_on_youtube: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('earning_subscribe_on_youtube, data: ', data)
                },
            },
            earning_follow_on_pinterest: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('earning_follow_on_pinterest, data: ', data)
                },
            },
            redeem_apply_now: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('redeem_apply_now, data: ', data)
                },
            },
            myreward_use_it_now: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('myreward_use_it_now, data: ', data)
                },
            },
            viptier_purchase_more: {
                function: ({ data }) => {
                  // YOUR CUSTOM CODE GOES HERE
                  console.log('viptier_purchase_more, data: ', data)
                },
            },
        },
    }
</script>
</head>
```

### Event option  

#### Special Events*
***earning_complete_an_order*** or ***viptier_purchase_more***: If the event being listened to and execute as above example then BON Loyalty will stop redirect customer to default product listing page, then you have to write your own redirect or action code to mange! If you want to keep it as default, DON`T INIT the event listener for ***earning_complete_an_order*** or ***viptier_purchase_more***

***All Other Events***  
```run_original_function: false``` Stop running BON Loyalty built-in function and you can manage it yourself. Default value: ```true```
